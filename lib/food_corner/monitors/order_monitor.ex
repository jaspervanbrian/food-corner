  defmodule FoodCorner.Monitors.OrderMonitor do
    use GenServer

    alias FoodCorner.{
      EtsServer,
      Orders
    }

    def start_link(default) do
      GenServer.start_link(__MODULE__, default, name: __MODULE__)
    end

    def monitor(%{pid: pid, order: order}) do
      GenServer.call(__MODULE__, {:monitor, %{pid: pid, order: order}})
    end

    def start_order(order_id) do
      GenServer.call(__MODULE__, {:start_order, order_id})
    end

    def pause_order(order_id) do
      GenServer.call(__MODULE__, {:pause_order, order_id})
    end

    def correct_guess(order_id) do
      GenServer.call(__MODULE__, {:correct_guess_and_pause, order_id})
    end

    @impl true
    def init(_) do
      {:ok, %{}}
    end

    @impl true
    def handle_call({:monitor, %{pid: pid, order: order}}, _, state) do
      state[order.id]
      |> case do
        nil ->
          Process.monitor(pid)

          new_state = Map.put(state, order.id, %{pid: pid, timer: nil})

          EtsServer.insert(:order_time_left, order.id, order.time_left)

          {:reply, :ok, new_state}

        _ -> {:reply, :in_order, state}
      end
    end

    @impl true
    def handle_call({:start_order, order_id}, _, state) do
      state[order_id]
      |> case do
        nil -> {:reply, :not_found, state}

        %{pid: pid, timer: _timer} ->
          timer_ref = progress_timer(order_id)
          new_state = Map.put(state, order_id, %{pid: pid, timer: timer_ref})

          {:reply, :ok, new_state}
      end
    end

    @impl true
    def handle_info({:progress_timer, order_id}, state) do
      state[order_id]
      |> case do
        nil -> {:reply, :not_found, state}

        %{pid: pid, timer: timer} ->
          if timer !== :paused do
            if is_reference(timer) do
              Process.cancel_timer(timer)
            end

            time_left = EtsServer.lookup(:order_time_left, order_id)

            if time_left > 0 do
              time_left = time_left - 1

              EtsServer.insert(:order_time_left, order_id, time_left)

              :ok = Process.send(pid, {:tick, time_left}, [])

              timer_ref = progress_timer(order_id)
              new_state = Map.put(state, order_id, %{pid: pid, timer: timer_ref})

              {:noreply, new_state}
            else
              EtsServer.delete(:order_time_left, order_id)

              new_state = Map.delete(state, order_id)

              {:noreply, new_state}
            end
          else
            {:noreply, state}
          end
      end
    end

    @impl true
    def handle_info({:DOWN, _ref, :process, pid, _reason}, state) do
      order =
        state
        |> Enum.find(fn {_, val} -> val[:pid] == pid end)

      case order do
        {order_id, %{pid: order_pid, timer: timer}} ->
          if order_pid === pid && is_reference(timer) do
            Process.cancel_timer(timer)
          end

          time_left = EtsServer.lookup(:order_time_left, order_id)

          Games.get_order!(order_id)
          |> Games.update_order(%{time_left: time_left})

        EtsServer.delete(:order_time_left, order_id)

        new_state = Map.delete(state, order_id)

        {:noreply, new_state}

      _ -> {:noreply, state}
    end
  end

  defp progress_timer(order_id) do
    Process.send_after(self(), {:progress_timer, order_id}, 1000)
  end
end
