defmodule FoodCorner.Menu.MenuItem do
  use Ecto.Schema
  import Ecto.Changeset

  schema "menu_items" do
    field :calories, :float
    field :carbohydrates, :float
    field :cholesterol, :float
    field :description, :string
    field :fat, :float
    field :image_name, :string
    field :item_name, :string
    field :price, :float
    field :protein, :float
    field :saturated_fat, :float
    field :sodium, :float
    field :sugar, :float

    timestamps()
  end

  @doc false
  def changeset(menu_item, attrs) do
    menu_item
    |> cast(attrs, [:item_name, :price, :description, :image_name, :calories, :fat, :saturated_fat, :cholesterol, :sodium, :carbohydrates, :sugar, :protein])
    |> validate_required([:item_name, :price, :description, :image_name, :calories, :fat, :saturated_fat, :cholesterol, :sodium, :carbohydrates, :sugar, :protein])
  end
end
