defmodule FoodCorner.Repo do
  use Ecto.Repo,
    otp_app: :food_corner,
    adapter: Ecto.Adapters.Postgres
end
