defmodule FoodCorner.Orders do
  @moduledoc """
  The Orders context.
  """

  import Ecto.Query, warn: false
  alias FoodCorner.Repo

  alias FoodCorner.Orders.{Order, OrderItem}

  @doc """
  Returns the list of orders.

  ## Examples

      iex> list_orders()
      [%Order{}, ...]

  """
  def list_orders do
    Repo.all(Order)
  end

  @doc """
  Gets a single order.

  Raises `Ecto.NoResultsError` if the Order does not exist.

  ## Examples

      iex> get_order!(123)
      %Order{}

      iex> get_order!(456)
      ** (Ecto.NoResultsError)

  """
  def get_order!(id), do: Repo.get!(Order, id)

  @doc """
  Creates a order.

  ## Examples

      iex> create_order(%{field: value})
      {:ok, %Order{}}

      iex> create_order(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order(attrs \\ %{}) do
    %Order{}
    |> Order.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a order.

  ## Examples

      iex> update_order(order, %{field: new_value})
      {:ok, %Order{}}

      iex> update_order(order, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_order(%Order{} = order, attrs) do
    order
    |> Order.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a order.

  ## Examples

      iex> delete_order(order)
      {:ok, %Order{}}

      iex> delete_order(order)
      {:error, %Ecto.Changeset{}}

  """
  def delete_order(%Order{} = order) do
    Repo.delete(order)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking order changes.

  ## Examples

      iex> change_order(order)
      %Ecto.Changeset{data: %Order{}}

  """
  def change_order(%Order{} = order, attrs \\ %{}) do
    Order.changeset(order, attrs)
  end

  @doc """
  Creates an order_item.

  ## Examples

      iex> create_order_item(%{field: value})
      {:ok, %OrderItem{}}

      iex> create_order_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_order_item(attrs \\ %{}) do
    %OrderItem{}
    |> OrderItem.changeset(attrs)
    |> Repo.insert()
  end

  def create_order_with_order_items(order_attrs, order_items_attrs) do
    Ecto.Multi.new()
    |> Ecto.Multi.run(:order, fn repo, _multi ->
      %Order{}
      |> Order.changeset(order_attrs)
      |> Repo.insert()
    end)
    |> Ecto.Multi.run(:order_items, fn repo, %{order: order} ->
      Enum.reduce_while(order_items_attrs, {:ok, %{order_items: []}}, fn
        order_item_attr, {:ok, %{order_items: existing_order_items}} ->
          {"menu_item_" <> menu_item_id, quantity} = order_item_attr

          attr = %{
            menu_item_id: menu_item_id,
            quantity: quantity,
            order_id: order.id
          }

          %OrderItem{}
          |> OrderItem.changeset(attr)
          |> repo.insert()
          |> case do
            {:ok, saved_item} ->
              acc = %{
                order_items: [saved_item | existing_order_items],
              }

              {:cont, {:ok, acc}}

            error ->
              {:halt, error}
          end
      end)
    end)
    |> Repo.transaction()
  end

  def fetch_orders_by_status(status \\ :pending) do
    from(
      o in Order,
      where: o.status == ^status,
      preload: [
        {:order_items, [:menu_item]},
        :checkout
      ]
    )
    |> Repo.all()
  end

  def fetch_orders_by_ids(nil), do: []
  def fetch_orders_by_ids([]), do: []
  def fetch_orders_by_ids(ids) do
    from(
      o in Order,
      where: o.id in ^ids, 
      preload: [
        {:order_items, [:menu_item]},
        :checkout
      ]
    )
    |> Repo.all()
  end

  def preload_order_associations(%Order{} = order) do
    order
    |> Repo.preload([
      {:order_items, [:menu_item]},
      :checkout
    ])
  end
end
