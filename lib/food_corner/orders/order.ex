defmodule FoodCorner.Orders.Order do
  use Ecto.Schema
  import Ecto.Changeset
  alias FoodCorner.Orders.OrderItem
  alias FoodCorner.Checkouts.Checkout

  schema "orders" do
    field :customer_name, :string
    field :table_number, :string
    field :time_left, :integer
    field :takeaway, :boolean, default: false
    field :status, Ecto.Enum, values: [:pending, :preparing, :done, :cancelled], default: :pending
    belongs_to :checkout, Checkout
    has_many :order_items, OrderItem

    timestamps()
  end

  @doc false
  def changeset(order, attrs) do
    order
    |> cast(attrs, [:customer_name, :table_number, :takeaway, :checkout_id, :status, :time_left])
    |> validate_required([:customer_name, :table_number, :takeaway, :checkout_id, :status])
    |> validate_inclusion(:status, [:pending, :preparing, :done, :cancelled])
  end
end
