defmodule FoodCorner.Orders.OrderItem do
  use Ecto.Schema
  import Ecto.Changeset
  alias FoodCorner.Orders.Order
  alias FoodCorner.Menu.MenuItem

  schema "order_items" do
    field :quantity, :integer
    belongs_to :order, Order
    belongs_to :menu_item, MenuItem

    timestamps()
  end

  @doc false
  def changeset(order_item, attrs) do
    order_item
    |> cast(attrs, [:quantity, :order_id, :menu_item_id])
    |> validate_required([:quantity, :order_id, :menu_item_id])
  end
end
