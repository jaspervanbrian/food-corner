defmodule FoodCornerWeb.Layouts do
  use FoodCornerWeb, :html

  embed_templates "layouts/*"
end
