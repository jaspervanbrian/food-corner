defmodule FoodCornerWeb.LandingLive do
  use FoodCornerWeb, :live_view

  alias FoodCorner.{
    Menu,
    Checkouts,
    Orders
  }

  @impl true
  def mount(_params, _assigns, socket) do
    menu_items = Menu.list_menu_items
    checkout_form = Checkouts.change_checkout(%Checkouts.Checkout{}) |> to_form

    {:ok, assign(socket,
      menu_items: menu_items,
      order: %{
        "customer_name" => "",
        "table_number" => 1,
        "takeaway" => false,
        "order_items" => %{}
      },
      total_qty: 0,
      checkout_form: checkout_form,
      checkout: nil,
      payment_intent: nil,
      active_orders: [],
      active_order_ids: []
    )}
  end

  @impl true
  def handle_event("restore", %{"order" => nil, "active_orders" => nil}, socket), do: {:noreply, socket}
  def handle_event("restore", %{"order" => order, "active_order_ids" => active_order_ids}, %{assigns: assigns} = socket) do
    order = if order, do: order, else: assigns.order
    order_items = if is_map(order["order_items"]), do: order["order_items"], else: %{}

    total_qty = order_items
                |> Map.values()
                |> Enum.reduce(0, fn item_qty, total -> item_qty + total end)

    active_order_ids = if is_list(active_order_ids), do: active_order_ids, else: []
    active_orders = Orders.fetch_orders_by_ids(active_order_ids)

    {:noreply, assign(socket,
      order: order,
      total_qty: total_qty,
      active_orders: active_orders,
      active_order_ids: active_order_ids,
    )}
  end

  def handle_event("add_to_order", %{"menu_item" => menu_item_id, "quantity" => quantity}, %{assigns: assigns} = socket) do
    %{
      order: order,
      menu_items: menu_items
    } = assigns

    order_items = if order["order_items"], do: order["order_items"], else: %{}
    menu_item = Enum.find(menu_items, fn item -> item.id == String.to_integer(menu_item_id) end)
    qty = String.to_integer(quantity)
    order_items = order_items
                  |> Map.update("menu_item_#{menu_item_id}", qty, fn existing_value ->
                    existing_value + qty
                  end)
    total_qty = order_items
                |> Map.values()
                |> Enum.reduce(0, fn item_qty, total -> item_qty + total end)

    order = Map.put(order, "order_items", order_items)

    socket = socket
             |> assign(order: order, total_qty: total_qty)
             |> put_flash(:info, "Added #{quantity} #{menu_item.item_name} to order.")
             |> push_event("update_order", %{order: order})

    {:noreply, socket}
  end
  def handle_event("add_to_order", _params, socket), do: {:noreply, socket}

  def handle_event("remove_from_order", %{"menu_item" => menu_item_id}, %{assigns: assigns} = socket) do
    %{
      order: %{
        "order_items" => order_items
      },
      menu_items: menu_items
    } = assigns

    order_items = order_items
                  |> Map.delete("menu_item_#{menu_item_id}")
    total_qty = order_items
                |> Map.values()
                |> Enum.reduce(0, fn item_qty, total -> item_qty + total end)

    order = %{assigns.order | "order_items" => order_items}

    socket = socket
             |> assign(order: order, total_qty: total_qty)
             |> push_event("update_order", %{order: order})

    {:noreply, socket}
  end
  def handle_event("remove_from_order", _params, socket), do: {:noreply, socket}

  def handle_event("update_checkout_form", %{"customer_name" => customer_name, "table_number" => table_number}, %{assigns: assigns} = socket) do
    takeaway = table_number === "Takeaway"
    order = %{
      assigns.order |
      "customer_name" => customer_name,
      "table_number" => table_number,
      "takeaway" => takeaway
    }

    socket = socket
             |> assign(order: order)
             |> push_event("update_order", %{order: order})

    {:noreply, socket}
  end
  def handle_event("update_checkout_form", _params, socket), do: {:noreply, socket}

  def handle_event("checkout", _params, %{assigns: assigns} = socket) do
    checkout_form = assigns.checkout_form
    name = if String.trim(checkout_form[:name].value || "") != "", do: checkout_form[:name].value, else: assigns.order["customer_name"]

    checkout_changeset = checkout_form.source
    checkout_form = checkout_changeset
                    |> Ecto.Changeset.put_change(:name, name)
                    |> to_form

    {:noreply, assign(socket, checkout_form: checkout_form)}
  end
  def handle_event("checkout", _params, socket), do: {:noreply, socket}

  def handle_event("validate_order_payment", %{"checkout" => %{"name" => name, "email" => email}}, %{assigns: assigns} = socket) do
    checkout_changeset = assigns.checkout_form.source
    checkout_form = checkout_changeset
                    |> Ecto.Changeset.put_change(:name, name)
                    |> Ecto.Changeset.put_change(:email, email)
                    |> to_form

    socket = socket
             |> assign(checkout_form: checkout_form)
             |> push_event("update_checkout", %{"checkout" => %{"name" => name, "email" => email}})

    {:noreply, socket}
  end
  def handle_event("validate_order_payment", _params, socket), do: {:noreply, socket}

  def handle_event("proceed_order_payment", %{"checkout" => checkout_params}, %{assigns: assigns} = socket) do
    %{order: order, menu_items: menu_items} = assigns

    order_item_ids = Map.keys(order["order_items"]) |> Enum.map(fn "menu_item_" <> id -> id end)

    order_items = Enum.map(order_item_ids, fn item_id ->
      Enum.find(menu_items, fn menu_item ->
        menu_item.id == String.to_integer(item_id)
      end)
    end)

    subtotal = Enum.reduce(order_items, 0.00, fn item, total ->
      Float.round((item.price * (order["order_items"]["menu_item_#{item.id}"]/1)) + total, 2)
    end)

    checkout_params = Map.put(checkout_params, "amount", trunc(subtotal * 100))

    case Checkouts.create_checkout(checkout_params) do
      {:ok, checkout} ->
        send(self(), {:create_payment_intent, checkout}) # Run this async

        checkout_form = checkout
                        |> Checkouts.change_checkout()
                        |> to_form

        {:noreply, assign(socket, checkout: checkout, checkout_form: checkout_form)}
      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :changeset, changeset)}
    end
  end
  def handle_event("proceed_order_payment", _params, socket), do: {:noreply, socket}

  def handle_info({:create_payment_intent, %{email: email, name: name, amount: amount, currency: currency} = checkout}, socket) do
    with {:ok, stripe_customer} <- Stripe.Customer.create(%{email: email, name: name}),
         {:ok, payment_intent} <- Stripe.PaymentIntent.create(%{customer: stripe_customer.id, amount: amount, currency: currency}) do

      # Update the checkout
      Checkouts.update_checkout(checkout, %{payment_intent_id: payment_intent.id})

      {:noreply, assign(socket, payment_intent: payment_intent)}
    else
      err ->
        {:noreply, put_flash(socket, :error, "There was an error with Stripe.")}
    end
  end

  def handle_event("payment_success", %{"payment_method" => payment_method_id, "status" => status}, %{assigns: assigns} = socket) do
    %{
      checkout: checkout,
      order: order
    } = assigns
    # Update the checkout with the result
    {:ok, checkout} = Checkouts.update_checkout(checkout, %{payment_method_id: payment_method_id, status: status})

    {order_items_attrs, order_attrs} = Map.pop(assigns.order, "order_items")

    socket =
      order_attrs
      |> Map.merge(%{"status" => :pending, "checkout_id" => checkout.id})
      |> Orders.create_order_with_order_items(order_items_attrs)
      |> case do
        {:ok, %{order: saved_order}} ->
          new_assigns = [
            order: %{order | "order_items" => %{}},
            total_qty: 0,
            checkout: nil,
            payment_intent: nil,
            active_orders: [Orders.preload_order_associations(saved_order) | assigns.active_orders],
            active_order_ids: [saved_order.id | assigns.active_order_ids]
          ]

          socket = socket
                   |> assign(new_assigns)
                   |> push_event("update_order", %{
                     order: new_assigns[:order],
                   })
                   |> push_event("order_success", %{
                     active_order_ids: new_assigns[:active_order_ids]
                   })
                   |> put_flash(:info, "You have placed your order!")

          {:noreply, socket}

        err ->
          IO.inspect(err)
          {:noreply, put_flash(socket, :error, "There was an error with the checkout process.")}
      end
  end
  def handle_event("payment_success", _params, socket), do: {:noreply, socket}
end
