defmodule FoodCornerWeb.UserDashboardLive do
  use FoodCornerWeb, :live_view

  alias FoodCorner.{
    Accounts,
    Orders
  }

  @impl true
  def mount(_params, _session, socket) do
    socket = socket
             |> refresh_orders()
             |> assign(
               wait_time: 1,
               invalid_wait_time: false
             )

    {:ok, socket}
  end

  @impl true
  def handle_event("change_wait_time", %{"wait_time" => wait_time}, socket) do
    change_wait_time(wait_time, socket)
  end
  def handle_event("change_wait_time", %{"value" => wait_time}, socket) do
    change_wait_time(wait_time, socket)
  end

  def handle_event("confirm_order", %{"order_id" => order_id, "wait_time" => wait_time}, socket) do
    Orders.get_order!(order_id)
    |> Orders.update_order(%{
      time_left: wait_time,
      status: :preparing
    })

    socket = socket
             |> refresh_orders
             |> put_flash(:info, "Started preparations for Order ##{order_id}, estimated wait time: #{wait_time} minutes.")

    {:noreply, socket}
  end

  def handle_event("mark_order_as_done", %{"order_id" => order_id}, socket) do
    Orders.get_order!(order_id)
    |> Orders.update_order(%{
      status: :done
    })

    socket = socket
             |> refresh_orders
             |> put_flash(:info, "Order ##{order_id} is ready!")

    {:noreply, socket}
  end

  def handle_event("cancel_order", %{"order_id" => order_id}, socket) do
    Orders.get_order!(order_id)
    |> Orders.update_order(%{status: :cancelled})

    socket = socket
             |> refresh_orders
             |> put_flash(:info, "Cancelled Order ##{order_id}.")

    {:noreply, socket}
  end

  def change_wait_time(wait_time, %{assigns: assigns} = socket) do
    Integer.parse(wait_time)
    |> case do
      {num, _} ->
        {:noreply, assign(socket, wait_time: num, invalid_wait_time: false)}

      _ -> {:noreply, assign(socket, invalid_wait_time: true)}
    end
  end

  def refresh_orders(socket) do
    pending_orders = Orders.fetch_orders_by_status
    preparing_orders = Orders.fetch_orders_by_status(:preparing)
    done_orders = Orders.fetch_orders_by_status(:done)
    cancelled_orders = Orders.fetch_orders_by_status(:cancelled)

    assign(socket,
      pending_orders: pending_orders,
      preparing_orders: preparing_orders,
      done_orders: done_orders,
      cancelled_orders: cancelled_orders
    )
  end
end
