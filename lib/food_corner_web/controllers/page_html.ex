defmodule FoodCornerWeb.PageHTML do
  use FoodCornerWeb, :html

  embed_templates "page_html/*"
end
