defmodule FoodCorner.Repo.Migrations.AddStatusToOrdersTable do
  use Ecto.Migration

  def change do
    alter table("orders") do
      add :status, :string
    end
  end
end
