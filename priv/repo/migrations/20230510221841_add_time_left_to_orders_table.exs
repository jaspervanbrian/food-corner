defmodule FoodCorner.Repo.Migrations.AddTimeLeftToOrdersTable do
  use Ecto.Migration

  def change do
    alter table("orders") do
      add :time_left, :integer
    end
  end
end
