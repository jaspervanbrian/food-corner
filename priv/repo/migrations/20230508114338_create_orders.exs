defmodule FoodCorner.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders) do
      add :customer_name, :string
      add :table_number, :string
      add :takeaway, :boolean, default: false, null: false

      timestamps()
    end
  end
end
