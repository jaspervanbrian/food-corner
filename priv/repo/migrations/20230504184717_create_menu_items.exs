defmodule FoodCorner.Repo.Migrations.CreateMenuItems do
  use Ecto.Migration

  def change do
    create table(:menu_items) do
      add :item_name, :string
      add :price, :float
      add :description, :string
      add :image_name, :string
      add :calories, :float
      add :fat, :float
      add :saturated_fat, :float
      add :cholesterol, :float
      add :sodium, :float
      add :carbohydrates, :float
      add :sugar, :float
      add :protein, :float

      timestamps()
    end
  end
end
