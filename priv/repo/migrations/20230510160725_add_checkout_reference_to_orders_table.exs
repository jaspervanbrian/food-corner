defmodule FoodCorner.Repo.Migrations.AddCheckoutReferenceToOrdersTable do
  use Ecto.Migration

  def change do
    alter table("orders") do
      add :checkout_id, references(:checkouts, on_delete: :nothing)
    end

    create index(:orders, [:checkout_id])
  end
end
