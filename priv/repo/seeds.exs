# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     FoodCorner.Repo.insert!(%FoodCorner.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

menu_items = "priv/repo/data/menu_items.csv"
             |> File.stream!
             |> CSV.decode(headers: true)

Ecto.Multi.new()
|> Ecto.Multi.run(:import_menu_items, fn repo, _multi ->
  Enum.reduce_while(menu_items, {:ok, %{menu_items: []}}, fn
    menu_item, {:ok, %{menu_items: existing_menu_items}} ->
      {:ok, item} = menu_item

      %FoodCorner.Menu.MenuItem{}
      |> FoodCorner.Menu.MenuItem.changeset(item)
      |> repo.insert()
      |> case do
        {:ok, saved_item} ->
          acc = %{
            menu_items: [saved_item | existing_menu_items],
          }

          {:cont, {:ok, acc}}

        error ->
          {:halt, error}
      end
  end)
end)
|> FoodCorner.Repo.transaction()

%FoodCorner.Accounts.User{}
|> FoodCorner.Accounts.User.registration_changeset(%{email: "staff@foodcorner.app", password: "longpassword"})
|> FoodCorner.Repo.insert()
