import * as Cache from "../utils/Cache"

export const UserDashboard = {
  mounted() {
    window.addEventListener(`confirm_cancel_order`, (e) => {
      const orderId = e.target.getAttribute("phx-value")
      if (confirm(`Are you sure you want to cancel Order #${orderId}?`)) {
        this.liveSocket.execJS(e.target, e.target.getAttribute("phx-cancel-order"))
        this.pushEvent("cancel_order", { order_id: orderId })
      }
    })
  }
}
