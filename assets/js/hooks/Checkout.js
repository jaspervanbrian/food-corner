import * as Cache from "../utils/Cache"

export const Checkout = {
  mounted() {
    const successCallback = paymentIntent => {
      this.pushEvent('payment_success', paymentIntent)
    }

    init(this.el, successCallback)
  }
}

const init = (form, successCallback) => {
  const stripeKey = document.querySelector("meta[name='stripe-key']").getAttribute("content")
  const stripe = Stripe(stripeKey)
  const clientSecret = form.dataset.secret

  // Create an instance of Elements.
  const elements = stripe.elements();

  // Create an instance of the card Element.
  const card = elements.create('card', {style: style});

  // Add an instance of the card Element into the `card-element` <div>.
  card.mount('#card-element');

  // Handle real-time validation errors from the card Element.
  card.on('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
      displayError.textContent = event.error.message;
    } else {
      displayError.textContent = '';
    }
  });

  // Handle form submission.
  form.addEventListener('submit', function(event) {
    event.preventDefault()

    const submitButton = event.target.querySelector("button")
    const spinner = submitButton.querySelector(".spinner")
    const icon = submitButton.querySelector(".hero-credit-card")
    submitButton.setAttribute("disabled", true)
    spinner.classList.remove("hidden")
    icon.classList.add("hidden")

    stripe.confirmCardPayment(clientSecret, {
      payment_method: {
        card: card
      }
    }).then(function(result) {
      if (result.error) {
        // Show error to your customer (e.g., insufficient funds)
        console.log(result.error.message);
      } else {
        // The payment has been processed!
        if (result.paymentIntent.status === 'succeeded') {
          // Show a success message to your customer
          successCallback(result.paymentIntent)
        }
      }
    })
  })
}

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
const style = {
  base: {
    color: '#32325d',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
}
