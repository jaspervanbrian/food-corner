import * as Cache from "../utils/Cache"

export const InitInfo = {
  mounted() {
    const order = Cache.getFromCache("order")
    const activeOrderIds = Cache.getFromCache("activeOrderIds")
    this.pushEvent("restore", { order, active_order_ids: activeOrderIds })

    const checkout = Cache.getFromCache("checkout")
    this.pushEvent("validate_order_payment", { checkout })

    this.handleEvent("update_order", ({ order }) => Cache.setToCache("order", order))
    this.handleEvent("update_checkout", ({ checkout }) => Cache.setToCache("checkout", checkout))

    this.handleEvent("order_success", ({ active_order_ids }) => {
      Cache.setToCache("activeOrderIds", active_order_ids)
      const orderPaymentModalHeader = document.querySelector("#checkout-modal-header")
      const closePaymentModal = orderPaymentModalHeader.dataset.orderSuccess
      this.liveSocket.execJS(orderPaymentModalHeader, closePaymentModal)

      const activeOrdersElement = document.querySelector("#active-orders")
      const showSidebar = activeOrdersElement.dataset.sidebarShow
      this.liveSocket.execJS(activeOrdersElement, showSidebar)
    })
  }
}
