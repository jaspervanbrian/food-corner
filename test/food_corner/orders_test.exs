defmodule FoodCorner.OrdersTest do
  use FoodCorner.DataCase

  alias FoodCorner.Orders

  describe "orders" do
    alias FoodCorner.Orders.Order

    import FoodCorner.OrdersFixtures

    @invalid_attrs %{customer_name: nil, table_number: nil, takeaway: nil}

    test "list_orders/0 returns all orders" do
      order = order_fixture()
      assert Orders.list_orders() == [order]
    end

    test "get_order!/1 returns the order with given id" do
      order = order_fixture()
      assert Orders.get_order!(order.id) == order
    end

    test "create_order/1 with valid data creates a order" do
      valid_attrs = %{customer_name: "some customer_name", table_number: "some table_number", takeaway: true}

      assert {:ok, %Order{} = order} = Orders.create_order(valid_attrs)
      assert order.customer_name == "some customer_name"
      assert order.table_number == "some table_number"
      assert order.takeaway == true
    end

    test "create_order/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Orders.create_order(@invalid_attrs)
    end

    test "update_order/2 with valid data updates the order" do
      order = order_fixture()
      update_attrs = %{customer_name: "some updated customer_name", table_number: "some updated table_number", takeaway: false}

      assert {:ok, %Order{} = order} = Orders.update_order(order, update_attrs)
      assert order.customer_name == "some updated customer_name"
      assert order.table_number == "some updated table_number"
      assert order.takeaway == false
    end

    test "update_order/2 with invalid data returns error changeset" do
      order = order_fixture()
      assert {:error, %Ecto.Changeset{}} = Orders.update_order(order, @invalid_attrs)
      assert order == Orders.get_order!(order.id)
    end

    test "delete_order/1 deletes the order" do
      order = order_fixture()
      assert {:ok, %Order{}} = Orders.delete_order(order)
      assert_raise Ecto.NoResultsError, fn -> Orders.get_order!(order.id) end
    end

    test "change_order/1 returns a order changeset" do
      order = order_fixture()
      assert %Ecto.Changeset{} = Orders.change_order(order)
    end
  end
end
