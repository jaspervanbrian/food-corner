defmodule FoodCorner.MenuTest do
  use FoodCorner.DataCase

  alias FoodCorner.Menu

  describe "menu_items" do
    alias FoodCorner.Menu.MenuItem

    import FoodCorner.MenuFixtures

    @invalid_attrs %{calories: nil, carbohydrates: nil, cholesterol: nil, description: nil, fat: nil, image_name: nil, item_name: nil, price: nil, protein: nil, saturated_fat: nil, sodium: nil, sugar: nil}

    test "list_menu_items/0 returns all menu_items" do
      menu_item = menu_item_fixture()
      assert Menu.list_menu_items() == [menu_item]
    end

    test "get_menu_item!/1 returns the menu_item with given id" do
      menu_item = menu_item_fixture()
      assert Menu.get_menu_item!(menu_item.id) == menu_item
    end

    test "create_menu_item/1 with valid data creates a menu_item" do
      valid_attrs = %{calories: 120.5, carbohydrates: 120.5, cholesterol: 120.5, description: "some description", fat: 120.5, image_name: "some image_name", item_name: "some item_name", price: 120.5, protein: 120.5, saturated_fat: 120.5, sodium: 120.5, sugar: 120.5}

      assert {:ok, %MenuItem{} = menu_item} = Menu.create_menu_item(valid_attrs)
      assert menu_item.calories == 120.5
      assert menu_item.carbohydrates == 120.5
      assert menu_item.cholesterol == 120.5
      assert menu_item.description == "some description"
      assert menu_item.fat == 120.5
      assert menu_item.image_name == "some image_name"
      assert menu_item.item_name == "some item_name"
      assert menu_item.price == 120.5
      assert menu_item.protein == 120.5
      assert menu_item.saturated_fat == 120.5
      assert menu_item.sodium == 120.5
      assert menu_item.sugar == 120.5
    end

    test "create_menu_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Menu.create_menu_item(@invalid_attrs)
    end

    test "update_menu_item/2 with valid data updates the menu_item" do
      menu_item = menu_item_fixture()
      update_attrs = %{calories: 456.7, carbohydrates: 456.7, cholesterol: 456.7, description: "some updated description", fat: 456.7, image_name: "some updated image_name", item_name: "some updated item_name", price: 456.7, protein: 456.7, saturated_fat: 456.7, sodium: 456.7, sugar: 456.7}

      assert {:ok, %MenuItem{} = menu_item} = Menu.update_menu_item(menu_item, update_attrs)
      assert menu_item.calories == 456.7
      assert menu_item.carbohydrates == 456.7
      assert menu_item.cholesterol == 456.7
      assert menu_item.description == "some updated description"
      assert menu_item.fat == 456.7
      assert menu_item.image_name == "some updated image_name"
      assert menu_item.item_name == "some updated item_name"
      assert menu_item.price == 456.7
      assert menu_item.protein == 456.7
      assert menu_item.saturated_fat == 456.7
      assert menu_item.sodium == 456.7
      assert menu_item.sugar == 456.7
    end

    test "update_menu_item/2 with invalid data returns error changeset" do
      menu_item = menu_item_fixture()
      assert {:error, %Ecto.Changeset{}} = Menu.update_menu_item(menu_item, @invalid_attrs)
      assert menu_item == Menu.get_menu_item!(menu_item.id)
    end

    test "delete_menu_item/1 deletes the menu_item" do
      menu_item = menu_item_fixture()
      assert {:ok, %MenuItem{}} = Menu.delete_menu_item(menu_item)
      assert_raise Ecto.NoResultsError, fn -> Menu.get_menu_item!(menu_item.id) end
    end

    test "change_menu_item/1 returns a menu_item changeset" do
      menu_item = menu_item_fixture()
      assert %Ecto.Changeset{} = Menu.change_menu_item(menu_item)
    end
  end
end
