defmodule FoodCorner.OrdersFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `FoodCorner.Orders` context.
  """

  @doc """
  Generate a order.
  """
  def order_fixture(attrs \\ %{}) do
    {:ok, order} =
      attrs
      |> Enum.into(%{
        customer_name: "some customer_name",
        table_number: "some table_number",
        takeaway: true
      })
      |> FoodCorner.Orders.create_order()

    order
  end
end
