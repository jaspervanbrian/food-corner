defmodule FoodCorner.MenuFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `FoodCorner.Menu` context.
  """

  @doc """
  Generate a menu_item.
  """
  def menu_item_fixture(attrs \\ %{}) do
    {:ok, menu_item} =
      attrs
      |> Enum.into(%{
        calories: 120.5,
        carbohydrates: 120.5,
        cholesterol: 120.5,
        description: "some description",
        fat: 120.5,
        image_name: "some image_name",
        item_name: "some item_name",
        price: 120.5,
        protein: 120.5,
        saturated_fat: 120.5,
        sodium: 120.5,
        sugar: 120.5
      })
      |> FoodCorner.Menu.create_menu_item()

    menu_item
  end
end
